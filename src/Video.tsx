import {Composition} from 'remotion';
import {FirstVideo} from './FirstVideo';
import configData from "../config.json";
import {SecondVideo} from "./SecondVideo";


export const RemotionVideo: React.FC = () => {
	return (
		<>
			<Composition
				id="FirstVideo"
				component={FirstVideo}
				durationInFrames={350}
				fps={30}
				width={1920}
				height={1080}
				defaultProps={{
					titleText: configData.text.start_text[0],
					titleColor:configData.color[3],
				}}
			/>
			<Composition
				id="SecondVideo"
				component={SecondVideo}
				durationInFrames={350}
				fps={25}
				width={720}
				height={1280}
				defaultProps={{
					titleText: configData.text.start_text[0],
					titleColor:configData.color[3],
				}}
			/>

		</>
	);
};
