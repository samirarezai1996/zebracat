import {Audio, Img, spring, staticFile, Video} from 'remotion';
import {
	AbsoluteFill,
	interpolate,
	Sequence,
	useCurrentFrame,
	useVideoConfig,
} from 'remotion';
import {Subtitle} from './components/SecondVideo/Subtitle';
import {Title} from './components/SecondVideo/Title';
import configData from "../config.json";
import {TitleBouncy} from "./components/SecondVideo/TitleBouncy";
import {TitleGradient} from "./components/SecondVideo/TitleGradient";
import audio from "../public/Sunsets 15s.mp3";
import React from "react";

const videoStyle: React.CSSProperties = {
	height: "100%",
	width: "100%",
	objectFit:"cover",
	objectPosition:'center'
};
export const SecondVideo: React.FC<{
	titleText: string;
	titleColor: string;
}> = ({titleText, titleColor}) => {
	const frame = useCurrentFrame();
	const {durationInFrames} = useVideoConfig();

	// Fade out the animation at the end
	const opacity = interpolate(
		frame,
		[durationInFrames - 25, durationInFrames - 15],
		[1, 0],
		{
			extrapolateLeft: 'clamp',
			extrapolateRight: 'clamp',
		}
	);

	return (
		<AbsoluteFill style={{backgroundColor: 'black'}}>
			<AbsoluteFill style={{opacity}}>
				<AbsoluteFill>
					<Audio src={audio}  />
				</AbsoluteFill>
				<AbsoluteFill>
					<Video src={staticFile(`footage/${configData.footage[7]}`)} style={videoStyle} muted/>
				</AbsoluteFill>
				<Sequence from={35}>
					<Title titleText={titleText} titleColor={titleColor} />
				</Sequence>
				<Sequence from={100}>
				<AbsoluteFill>
					<Video src={staticFile(`footage/${configData.footage[9]}`)} style={videoStyle} muted/>
				</AbsoluteFill>
				</Sequence>
				<Sequence from={120}>
					<TitleBouncy titleText={configData.text.middle_text[0].main} titleColor={configData.color[2]} />
					<Subtitle />
				</Sequence>
				<Sequence from={200}>
					<AbsoluteFill>
						<Video src={staticFile(`footage/${configData.footage[6]}`)} style={videoStyle} muted/>
					</AbsoluteFill>
				</Sequence>
				<Sequence from={220}>
					<TitleGradient titleText={configData.text.end_text[0]} titleColorOne={configData.color[3]} titleColorTow={configData.color[2]} />
				</Sequence>
			</AbsoluteFill>
		</AbsoluteFill>
	);
};
