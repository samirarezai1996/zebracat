import {Audio, staticFile, Video} from 'remotion';
import {
	AbsoluteFill,
	interpolate,
	Sequence,
	useCurrentFrame,
	useVideoConfig,
} from 'remotion';
import {Subtitle} from './components/FirstVideo/Subtitle';
import {Title} from './components/FirstVideo/Title';
import configData from "../config.json";
import {TitleBouncy} from "./components/FirstVideo/TitleBouncy";
import {TitleGradient} from "./components/FirstVideo/TitleGradient";
import audio from "../public/Sunsets 15s.mp3";
import React from "react";

const videoStyle: React.CSSProperties = {
	height: "100%",
	width: "100%",
	objectFit:"cover",
	objectPosition:'center'
};
export const FirstVideo: React.FC<{
	titleText: string;
	titleColor: string;
}> = ({titleText, titleColor}) => {
	const frame = useCurrentFrame();
	const {durationInFrames, fps} = useVideoConfig();

	// Fade out the animation at the end
	const opacity = interpolate(
		frame,
		[durationInFrames - 25, durationInFrames - 15],
		[1, 0],
		{
			extrapolateLeft: 'clamp',
			extrapolateRight: 'clamp',
		}
	);

	return (
		<AbsoluteFill style={{backgroundColor: 'black'}}>
			<AbsoluteFill style={{opacity}}>
				<AbsoluteFill>
					<Audio src={audio}  />
				</AbsoluteFill><AbsoluteFill>
					<Video src={staticFile(`footage/${configData.footage[0]}`)} style={videoStyle} />
				</AbsoluteFill>
				<Sequence from={35}>
					<Title titleText={titleText} titleColor={titleColor} />
				</Sequence>
				<Sequence from={100}>
				<AbsoluteFill>
					<Video src={staticFile(`footage/${configData.footage[1]}`)} style={videoStyle} />
				</AbsoluteFill>
				</Sequence>
				<Sequence from={120}>
					<TitleBouncy titleText={configData.text.middle_text[0].main} titleColor={configData.color[0]} />
					<Subtitle />
				</Sequence>
				<Sequence from={250}>
					<AbsoluteFill>
						<Video src={staticFile(`footage/${configData.footage[2]}`)} style={videoStyle} />
					</AbsoluteFill>
				</Sequence>
				<Sequence from={270}>
					<TitleGradient titleText={configData.text.end_text[0]} titleColorOne={configData.color[0]} titleColorTow={configData.color[3]} />
				</Sequence>
			</AbsoluteFill>
		</AbsoluteFill>
	);
};
