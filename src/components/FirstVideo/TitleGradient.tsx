import React from 'react';
import {Easing, interpolate, interpolateColors, useCurrentFrame} from 'remotion';
import configData from "../../../config.json";

const title: React.CSSProperties = {
    fontFamily: configData.main_font,
    fontWeight: 'bold',
    fontSize: 100,
    textAlign: 'center',
    position: 'absolute',
    bottom: "50%",
    width: '100%',
};

export const TitleGradient: React.FC<{
    titleText: string;
    titleColorOne: string;
    titleColorTow: string;
}> = ({titleText, titleColorOne,titleColorTow}) => {
    const frame = useCurrentFrame();

    const interpolated = interpolate(frame, [0, 100], [0, 1], {
        easing: Easing.bezier(0.8, 0.22, 0.96, 0.65),
        extrapolateLeft: "clamp",
        extrapolateRight: "clamp",
    });
    const color = interpolateColors(frame, [0, 20], [titleColorOne, titleColorTow]);
    return (
        <h1 style={title}>
		<span
            style={{
                transform: `scale(${interpolated})`,
                color
            }}
        >
						{titleText}
					</span>
        </h1>
    );
};
