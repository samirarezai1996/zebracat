import React from 'react';
import {Easing, interpolate, useCurrentFrame} from 'remotion';
import configData from "../../../config.json";

const title: React.CSSProperties = {
    fontFamily: configData.main_font,
    fontWeight: 'bold',
    fontSize: 100,
    textAlign: 'center',
    position: 'absolute',
    bottom: "40%",
    width: '100%',
};

export const TitleBouncy: React.FC<{
    titleText: string;
    titleColor: string;
}> = ({titleText, titleColor}) => {
    const frame = useCurrentFrame();

    const interpolated = interpolate(frame, [0, 100], [0, 1], {
        easing: Easing.bezier(0.8, 0.22, 0.96, 0.65),
        extrapolateLeft: "clamp",
        extrapolateRight: "clamp",
    });
    return (
        <h1 style={title}>
		<span
            style={{
                transform: `scale(${interpolated})`,
                opacity: interpolated,
                color: titleColor
            }}
        >
						{titleText}
					</span>
        </h1>
    );
};
