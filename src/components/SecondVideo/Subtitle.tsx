import React from 'react';
import {interpolate, useCurrentFrame} from 'remotion';
import configData from "../../../config.json";

const subtitle: React.CSSProperties = {
	fontFamily: configData.secondary_font,
	fontSize: 40,
	textAlign: 'center',
	position: 'absolute',
	bottom: "30%",
	width: '100%',
	color: configData.color[2],
};

export const Subtitle: React.FC = () => {
	const frame = useCurrentFrame();
	const opacity = interpolate(frame, [0, 30], [0, 1]);
	return (
		<div style={{...subtitle, opacity}}>
			{configData.text.middle_text[0].secondary}
		</div>
	);
};
